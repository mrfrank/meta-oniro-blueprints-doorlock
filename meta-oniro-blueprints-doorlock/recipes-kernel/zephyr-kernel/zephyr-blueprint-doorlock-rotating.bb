# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-kernel/zephyr-kernel/zephyr-blueprint-doorlock.bb

SUMMARY = "Doorlock blueprint image - Rotating doorlock variant"
DESCRIPTION = "Zephyr based image for handling door locks"
LICENSE = "Apache-2.0"

EXTRA_OECMAKE:append = ' -DCONFIG_LED_DOORLOCK=n -DCONFIG_ROTATING_DOORLOCK=y'
